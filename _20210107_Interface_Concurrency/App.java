package _20210107_Interface_Concurrency;

public class App {

    public static void main(String[] args) {
        System.out.printf("%s Started %n", Thread.currentThread().getName());
        WorkerClass worker1 = new WorkerClass();

        Thread t1 = new Thread(worker1);
        t1.start();

        Thread t2 = new Thread(worker1);
        t2.start();

        Thread t3 = new Thread(worker1);
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.printf("Counted %d times %n",DataStorage.counter);
    }

}

class WorkerClass implements Runnable{
    public void work(){
        for (int i = 0; i < 100000; i++) {
            DataStorage.counter++;
            //System.out.printf("%s Added +1 %n",Thread.currentThread().getName());
        }
    }

    @Override
    public void run() {
        work();
    }
}

class DataStorage {
    public static int counter = 0;
}
